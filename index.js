const students = [
    {
        id: 1,
        name: 'Aluno 1',
    }, {
        id: 2,
        name: 'Aluno 2',
    }, {
        id: 3,
        name: 'Aluno 3',
    }, {
        id: 4,
        name: 'Aluno 4',
    }, {
        id: 5,
        name: 'Aluno 5'
    }
];

exports.handler = async (event) => {
    let body = students;

    if (event && event.pathParameters) {
        const { id } = event.pathParameters;
        if (id) {
            const student = students.find(student => student.id == id);
            body = JSON.stringify(student);
        }
    }

    return {
        body
    };
};
